-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Gostitelj: 127.0.0.1
-- Čas nastanka: 15. sep 2016 ob 14.27
-- Različica strežnika: 10.1.10-MariaDB
-- Različica PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Zbirka podatkov: `osebe`
--

-- --------------------------------------------------------

--
-- Struktura tabele `share_txt_uporabniki`
--

CREATE TABLE `share_txt_uporabniki` (
  `ID_UPB` int(11) NOT NULL,
  `USR_NAME` varchar(100) NOT NULL,
  `USR_PASS` varchar(150) NOT NULL,
  `USR_IME` varchar(100) NOT NULL,
  `AKTIVEN` varchar(1) NOT NULL DEFAULT 'D',
  `USR_VLOGA` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Odloži podatke za tabelo `share_txt_uporabniki`
--

INSERT INTO `share_txt_uporabniki` (`ID_UPB`, `USR_NAME`, `USR_PASS`, `USR_IME`, `AKTIVEN`, `USR_VLOGA`) VALUES
(1, 'root', 'geslo', 'Admin', 'D', 'admin'),
(2, 'uporabnik', 'uporabnik', 'Navaden uporabnik', 'D', ''),
(13, 'tesla', '$2a$10$WH.hpo09CDwDuhxsepqqiuL21N8XiRyKqhBAmQSfGWFFWC4lDSBUC', 'tesla', 'D', '');

--
-- Indeksi zavrženih tabel
--

--
-- Indeksi tabele `share_txt_uporabniki`
--
ALTER TABLE `share_txt_uporabniki`
  ADD PRIMARY KEY (`ID_UPB`);

--
-- AUTO_INCREMENT zavrženih tabel
--

--
-- AUTO_INCREMENT tabele `share_txt_uporabniki`
--
ALTER TABLE `share_txt_uporabniki`
  MODIFY `ID_UPB` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
