﻿var mysql = require('mysql');
var db = null;

function connectDatabase() {
	if (!db) {
		db = mysql.createConnection({
			host : "localhost",
			user : "uporabnik",
			password : "uporabnik",
			database : "osebe"
		});
		
		db.connect(function (err) {
			if (!err) {
				console.log('Database is connected!');
			} else {
				console.log('Error connecting database!');
			}
		});
	}
	return db;
}

module.exports = connectDatabase();