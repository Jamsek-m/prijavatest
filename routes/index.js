﻿var express = require('express');
var router = express.Router();
var connection = require('../config/database.js');
var formidable = require('formidable');
var bodyParser = require('body-parser');
var session = require('express-session');
var enkript = require('bcrypt-nodejs');

prijavljeniUporabnik = "";



/* GET home page. */
router.get('/', function (req, res, next) {
	
	if (req.session.prijavljeniUporabnik == "" || !req.session.prijavljeniUporabnik) { //dodaj klicaj za delovanje
		res.redirect('/login');
	} else {
		
		var users = {
			upime : "",
			upgeslo : ""
		};
		
		connection.query('SELECT USR_NAME as upime, USR_PASS as upgeslo FROM share_txt_uporabniki', function (err, rows , fields) {
			if (!err) {
				users = rows;
				res.render('index', {sporocilo : ""})
			} else {
				console.log("Napaka!", err);
			}
		});
	}
});

router.get('/login', function (req, res, next) {
	res.render('login');
});

function preveriUporabnike(vnos_ime, vnos_geslo, rows, req){
	for (var i = 0; i < rows.length; i++) {
		if (vnos_ime == rows[i].upime && vnos_geslo == rows[i].upgeslo) {
			if (rows[i].aktiven == 'D') {
				console.log("User " + vnos_ime + " is signed in!");
				req.session.prijavljeniUporabnik = vnos_ime;
				return true;
			}
		}
	}
	console.log("Requested user (" + vnos_ime + ") is not able to log in!");
	return false;
}


/*function preveriUporabnike(vnos_ime, vnos_geslo, rows, req) {
	for (var i = 0; i < rows.length; i++) {
		enkript.compare(vnos_geslo, rows[i].upgeslo, function (err, res) {
			console.log("i: " + i + ", rows: " + rows[i]);
			if (res == false) {
				return false;
			}
			if (rows[i].upime == vnos_ime) {
				if (rows[i].aktiven == 'D') {
					req.session.prijavljeniUporabnik = vnos_ime;
					console.log("Uporabnik " + vnos_ime + " uspešno prijavljen!");
					return true;
				}
			}
		});
	}
	console.log("zahtevan je bil napačen uporabnik.");
	return false;
}*/

router.post('/checklogin', function (req, res, next) {
	// uporabniško ime in geslo ki si ga dobil od clienta (AJAX request na /checkLogin)
	
	var uporabniskoIme = req.body.username;
	var geslo = req.body.password;
	
	var ajaxOdgovor;
	
	connection.query('SELECT USR_NAME as upime, USR_PASS as upgeslo, AKTIVEN as aktiven, USR_VLOGA as upvloga FROM share_txt_uporabniki', function (err, rows) {
		if (err) {
			res.json({
				uspeh: false,
				odgovor: "Napaka pri izvajanju SELECT stavka!"
			});
		}
		
		var rezultatPrijave = preveriUporabnike(uporabniskoIme, geslo, rows, req);
		
		if (rezultatPrijave) {
			res.json({
				veljavnost: true,
				preusmeritev: "/"
			});
		} else {
			res.json({
				veljavnost: false,
				odgovor: "/login"
			});
		}
	});
});

router.get('/odjava', function (req, res, next) {
	req.session.prijavljeniUporabnik = "";
	res.redirect('/login');
});

/*router.post('/hash', function (req, res, next) {
	
	enkript.hash("bacon", null, null, function (err, hash) {
		// Store hash in your password DB.
	});
	
	// Load hash from your password DB.
	bcrypt.compare("bacon", hash, function (err, res) {
    // res == true
	});
	bcrypt.compare("veggies", hash, function (err, res) {
    // res = false
	});

	var hash = enkript.hashSync("Slanina");
	var rez = enkript.compareSync("Test", hash);
	console.log("HASH: ", rez);

	res.render('index');

});*/

router.post('/dodajUporabnika', function (req, res, next) {
	var form = new formidable.IncomingForm();
	form.parse(req, function (err, polja, datoteke) {
		if (err) throw err;

		var ime = polja.Ime;
		var geslo = polja.Geslo;
		var naziv = polja.DisplayIme;
		var tip = polja.Tip;
		
		enkript.hash(geslo, null, null, function (err, hash) {
		// Store hash in your password DB.
			var sql = "INSERT INTO share_txt_uporabniki (USR_NAME, USR_PASS, USR_IME, USR_VLOGA) VALUES('" + ime + "', '" + hash + "', '" + naziv + "', '" + tip + "')";
			connection.query(sql, function (err, rows) {
				var message = "";
				if (err) {
					message = "Napaka pri insertu";
				} else {
					message = "Uporabnik dodan!";
				}
				res.render('index' , { sporocilo: message });
			});
		});
		
	});


});




module.exports = router;
